<?php declare(strict_types=1);

namespace JMSE\Manufacturer\Controller;

use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\ContainsFilter;
use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\Routing\Annotation\Route;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\Context;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

/**
 * @Route(defaults={"_routeScope"={"store-api"}})
 */
class ManufacturersController extends StorefrontController
{
    /**
     * @Route("/store-api/manufacturers", name="store-api.manufacturers", methods={"GET"})
     */
    public function manufacturers(Context $context)
    {
        $manus = $this->container->get('product_manufacturer.repository');
        $criteria = new Criteria();
        $criteria->setLimit(100);

        return $this->json(['message'=> $manus->search($criteria,$context)]);
    }

    /**
     * @Route("/store-api/manufacturer/search/{query}", name="store-api.manufacturer.search", methods={"GET"})
     */
    public function search(string $query, Context $context)
    {
        $manus = $this->container->get('product_manufacturer.repository');
        $criteria = new Criteria();
        $criteria->setLimit(10);
        $criteria->addFilter(new ContainsFilter('name', $query));

        return $this->json(['message'=> $manus->search($criteria,$context)]);
    }

    /**
     * @Route("/store-api/manufacturer/{id}", name="store-api.manufacturer", methods={"GET"})
     */
    public function manufacturer(string $id, Context $context)
    {
        $manus = $this->container->get('product_manufacturer.repository');
        $criteria = new Criteria([$id]);
        $criteria->setLimit(1);

        $view = json_decode(json_encode($manus->search($criteria,$context), JSON_FORCE_OBJECT))->elements;

        return $this->json(['message'=> $view]);
    }
}